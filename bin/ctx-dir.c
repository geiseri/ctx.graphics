#include "local.conf"
#if CTX_CSS

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "ctx.h"

#if CTX_BIN_BUNDLE
int ctx_dir_main (int argc, char **argv)
#else
int main (int argc, char **argv)
#endif
{
  Ctx *ctx = ctx_new (-1, -1, NULL);
  Css *itk = css_new (ctx);


  css_destroy (itk);
  ctx_destroy (ctx);
  return 0;
}

#else

// dummies
#if CTX_BIN_BUNDLE
int ctx_dir_main (int argc, char **argv)
#else
int main (int argc, char **argv)
#endif
{
  return -1;
}

#endif

