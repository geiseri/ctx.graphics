/* tcp/udp listening and integration code,
 *
 *
 */

#include "ctx.h"
#include <unistd.h>
#include <errno.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/time.h>


int tcp_mode = 0;

static void do_quit (CtxEvent *event, void *d1, void *d2)
{
  ctx_exit (event->ctx);
}

CtxParser *net_parser = NULL;

#include <sys/socket.h>
#include <netdb.h>
int PORT = 6150;
//#include <sys.h>

static void do_retransmit(const int sock)
{
    int len;
    char rx_buffer[128];
    
    do {
        len = recv(sock, rx_buffer, sizeof(rx_buffer) - 1, 0);
        if (len < 0) {
            fprintf(stderr, "Error occurred during receiving: errno %d\n", errno);
        } else if (len == 0) {
            fprintf(stderr, "Connection closed\n");
        } else {
            rx_buffer[len] = 0; // Null-terminate whatever is received and treat it like a string
            //fprintf(stderr, "Received %d bytes: %s\n", len, rx_buffer);


            ctx_parser_feed_bytes (net_parser, rx_buffer, len);
        }
    } while (len > 0);
}



void net_start_frame (Ctx *ctx, void *data)
{
  ctx_start_frame (ctx);
}

void net_end_frame (Ctx *ctx, void *data)
{
  ctx_end_frame (ctx);
}

int sock ;
int tcpsock;

void net_response (Ctx *ctx, void *data, char *response, int len)
{
 // fwrite (response, 1, len, net_out_file);
            int to_write = len;
            while (to_write > 0) {
                int written = send(tcpsock, response + (len - to_write), to_write, 0);
                if (written < 0) {
                    fprintf(stderr, "Error occurred during sending: errno %d\n", errno);
                    // Failed to retransmit, giving up
                    return;
                }
                to_write -= written;
            }
}


static void net_server_task(void *pvParameters)
{
    char rx_buffer[1200];
    char addr_str[128];
    int addr_family = (size_t)pvParameters;
    int ip_protocol = 0;
    struct sockaddr_in6 dest_addr;

    int first = 1;
    while (1) {

#if 1
        if (addr_family == AF_INET) {
            struct sockaddr_in *dest_addr_ip4 = (struct sockaddr_in *)&dest_addr;
            dest_addr_ip4->sin_addr.s_addr = htonl(INADDR_ANY);
            dest_addr_ip4->sin_family = AF_INET;
            dest_addr_ip4->sin_port = htons(PORT);
            ip_protocol = IPPROTO_IP;
#if 0
        } else if (addr_family == AF_INET6) {
            bzero(&dest_addr.sin6_addr.un, sizeof(dest_addr.sin6_addr.un));
            dest_addr.sin6_family = AF_INET6;
            dest_addr.sin6_port = htons(PORT);
            ip_protocol = IPPROTO_IPV6;
#endif
        }
#endif
            ip_protocol = IPPROTO_IP;

        if (tcp_mode)
        { if (first) sock = socket(addr_family, SOCK_STREAM, ip_protocol);
        }
        else
        {
          sock = socket(addr_family, SOCK_DGRAM, ip_protocol);
        }
        int opt = 1;
        setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));
        if (sock < 0) {
            fprintf(stderr, "Unable to create socket: errno %d", errno);
            break;
        }
        printf("Socket created\n");

        int err;
        if (tcp_mode)
        {
          if (first)
          {
          err = bind(sock, (struct sockaddr *)&dest_addr, sizeof(dest_addr));
          if (err != 0) {
        fprintf (stderr, "Socket unable to bind: errno %d\n", errno);
        return;
          }

        fprintf(stderr, "Socket bound, port %d\n", PORT);
          }
 
        err = listen(sock, 1);
    if (err != 0) {
        fprintf(stderr, "Error occurred during listen: errno %d\n", errno);
        return;
    }

     struct sockaddr_storage source_addr; // Large enough for both IPv4 or IPv6
        socklen_t addr_len = sizeof(source_addr);
        tcpsock = accept(sock, (struct sockaddr *)&source_addr, &addr_len);
        if (tcpsock < 0) {
            fprintf(stderr, "Unable to accept connection: errno %d\n", errno);
            break;
        }
         int keepAlive = 1;
#if 0
    int keepIdle = 5;
    int keepInterval = 5;
    int keepCount = 3;
#endif

        // Set tcp keepalive option
        setsockopt(tcpsock, SOL_SOCKET, SO_KEEPALIVE, &keepAlive, sizeof(int));
        //setsockopt(tcpsock, IPPROTO_TCP, TCP_KEEPIDLE, &keepIdle, sizeof(int));
        //setsockopt(tcpsock, IPPROTO_TCP, TCP_KEEPINTVL, &keepInterval, sizeof(int));
        //setsockopt(tcpsock, IPPROTO_TCP, TCP_KEEPCNT, &keepCount, sizeof(int));

        fprintf (stderr, "socket accepted connection\n");

        do_retransmit(tcpsock);


        shutdown (tcpsock, SHUT_RDWR);
        close (tcpsock);
        first = 0;

        }
        else
        {

        // Set timeout
        struct timeval timeout;
        timeout.tv_sec = 10;
        timeout.tv_usec = 0;
        setsockopt (sock, SOL_SOCKET, SO_RCVTIMEO, &timeout, sizeof timeout);

        int err = bind(sock, (struct sockaddr *)&dest_addr, sizeof(dest_addr));
        if (err < 0) {
            fprintf(stderr, "Socket unable to bind: errno %d", errno);
        }
        printf("Socket bound, port %d", PORT);

        struct sockaddr_storage source_addr; // Large enough for both IPv4 or IPv6
        socklen_t socklen = sizeof(source_addr);

#if defined(CONFIG_LWIP_NETBUF_RECVINFO) && !defined(CONFIG_EXAMPLE_IPV6)
        struct iovec iov;
        struct msghdr msg;
        struct cmsghdr *cmsgtmp;
        u8_t cmsg_buf[CMSG_SPACE(sizeof(struct in_pktinfo))];

        iov.iov_base = rx_buffer;
        iov.iov_len = sizeof(rx_buffer);
        msg.msg_control = cmsg_buf;
        msg.msg_controllen = sizeof(cmsg_buf);
        msg.msg_flags = 0;
        msg.msg_iov = &iov;
        msg.msg_iovlen = 1;
        msg.msg_name = (struct sockaddr *)&source_addr;
        msg.msg_namelen = socklen;
#endif

        while (1) {
#if defined(CONFIG_LWIP_NETBUF_RECVINFO) && !defined(CONFIG_EXAMPLE_IPV6)
            int len = recvmsg(sock, &msg, 0);
#else
            int len = recvfrom(sock, rx_buffer, sizeof(rx_buffer) - 1, 0, (struct sockaddr *)&source_addr, &socklen);
#endif
            // Error occurred during receiving
            if (len < 0) {
                fprintf(stderr, "recvfrom failed: errno %d", errno);
                break;
            }
            // Data received
            else {
                // Get the sender's ip address as string
                if (source_addr.ss_family == PF_INET) {
                    //inet_ntoa_r(((struct sockaddr_in *)&source_addr)->sin_addr, addr_str, sizeof(addr_str) - 1);
                    char *tmp = inet_ntoa(((struct sockaddr_in *)&source_addr)->sin_addr);
                    strcpy (addr_str, tmp);
#if defined(CONFIG_LWIP_NETBUF_RECVINFO) && !defined(CONFIG_EXAMPLE_IPV6)
                    for ( cmsgtmp = CMSG_FIRSTHDR(&msg); cmsgtmp != NULL; cmsgtmp = CMSG_NXTHDR(&msg, cmsgtmp) ) {
                        if ( cmsgtmp->cmsg_level == IPPROTO_IP && cmsgtmp->cmsg_type == IP_PKTINFO ) {
                            struct in_pktinfo *pktinfo;
                            pktinfo = (struct in_pktinfo*)CMSG_DATA(cmsgtmp);
                            ESP_LOGI(TAG, "dest ip: %s", inet_ntoa(pktinfo->ipi_addr));
                        }
                    }
#endif
                } //else if (source_addr.ss_family == PF_INET6) {
                  //  inet6_ntoa_r(((struct sockaddr_in6 *)&source_addr)->sin6_addr, addr_str, sizeof(addr_str) - 1);
        //        }

                rx_buffer[len] = 0; // Null-terminate whatever we received and treat like a string...
                //printf("Received %d bytes from %s:\n", len, addr_str);
                //printf("%s\n", rx_buffer);
                ctx_parser_feed_bytes (net_parser, rx_buffer, len);

#if 0
                int err = sendto(sock, rx_buffer, len, 0, (struct sockaddr *)&source_addr, sizeof(source_addr));
                if (err < 0) {
                    fprintf (stderr, "Error occurred during sending: errno %d", errno);
                    break;
                }
#endif
            }
        }

        if (sock != -1) {
            printf("Shutting down socket and restarting...");
            shutdown(sock, 0);
            close(sock);
        }
    }
    }
    //vTaskDelete(NULL);
}


#if CTX_BIN_BUNDLE
int ctx_udp_main (int argc, char **argv)
{
  PORT = 6100;
  if (argv[1])
  {
    if (!strcmp (argv[1], "-h") || !strcmp(argv[1], "--help"))
    {
      printf ("ctx udp [PORT]\n");
      printf ("  listens for incoming udp connection on PORT defaulting to 6100\n");
      return 0;
    }
    PORT = atoi (argv[1]);
  }
  tcp_mode = 0;

  Ctx *ctx = ctx_new (640, 480, NULL);
  CtxParserConfig config = {
    .width       = ctx_width (ctx),
    .height      = ctx_height (ctx),
    .cell_width  = ctx_width (ctx)/30,
    .cell_height = ctx_width(ctx)/30*1.5,
    .start_frame = net_start_frame,
    .end_frame   = net_end_frame,
    .user_data   = &config,
  };
  net_parser = ctx_parser_new (ctx, &config);

  net_server_task((void *)AF_INET);
  ctx_parser_destroy (net_parser);
  ctx_destroy (ctx);
  return 0;
}
#endif

char gmessage[200]="";

void draw_splash (Ctx *inctx, const char *message)
{
  static Ctx *ctx = NULL;
  if (!ctx) ctx = inctx;

  if (message)
    strncpy (gmessage, message, sizeof(gmessage)-1);

  if (!ctx)
    return; 
 
  ctx_start_frame (ctx);
  ctx_rgb (ctx, 0, 0.3, 0);
  ctx_paint (ctx);
  ctx_logo (ctx, ctx_width(ctx)/2, ctx_height(ctx)/2, ctx_height(ctx)/2);
  if (gmessage[0])
  {
    ctx_save (ctx);
    ctx_move_to (ctx, ctx_width(ctx)/2, ctx_height(ctx)*0.8);
    ctx_font_size (ctx, ctx_height(ctx) * 0.1f);
    ctx_rgb (ctx,1.0,1.0,1.0);
    ctx_text_align (ctx, CTX_TEXT_ALIGN_CENTER);
    ctx_text (ctx, gmessage);
    ctx_restore (ctx);
  }

  {
    char info[128];
    float fsize = 10.0;
    int width = ctx_width (ctx);
    int height = ctx_height (ctx);
    sprintf (info, "%ix%i ", width, height);
    ctx_rectangle (ctx, 0,0, width, fsize);
    ctx_rgba (ctx, 0,0,0,0.5);
    ctx_fill (ctx);
    ctx_font_size (ctx, fsize);
    ctx_move_to (ctx, 0.0, fsize);
    ctx_rgb (ctx,1.0,1.0,1.0);
    ctx_text (ctx, info);
  }
  ctx_end_frame (ctx);
}

#ifdef ESP_PLATFORM
int app_main (void)
#else
#if CTX_BIN_BUNDLE
int ctx_tcp_main (int argc, char **argv)
#else
int main (int argc, char **argv)
#endif
#endif
{
  tcp_mode = 1;
  if (tcp_mode)
    PORT = 6150;
  else
    PORT = 6101;
#if !defined(ESP_PLATFORM)
  if (argv[1])
  {
    if (!strcmp (argv[1], "-h") || !strcmp(argv[1], "--help"))
    {
#if CTX_BIN_BUNDLE
      printf ("ctx tcp [PORT]\n");
#else
      printf ("ctx-net [PORT]\n");
#endif
      printf ("  listens for incoming tcp connection on PORT defaulting to 6150\n"
              "  with bash, this works to connect to the tcp-mode:\n"
              "    ./ctx-app >& /dev/tcp/localhost/6150 0>&1\n"
              "  or using traditional netcat:\n"
              "    nc localhost 6150 -e ./ctx-app\n");
      return 0;
    }
    PORT = atoi (argv[1]);
  }
#endif
  Ctx *ctx = ctx_new (-1, -1, NULL);
  draw_splash (ctx, NULL);

  CtxParserConfig config = {
    .width       = ctx_width (ctx),
    .height      = ctx_height (ctx),
    .cell_width  = ctx_width (ctx)/30,
    .cell_height = ctx_width(ctx)/30*1.5,
    .start_frame = net_start_frame,
    .end_frame   = net_end_frame,
    .user_data   = &config,
    .response    = tcp_mode?net_response:NULL,
    .flags       = tcp_mode?(CTX_FLAG_FORWARD_EVENTS | CTX_FLAG_HANDLE_ESCAPES):0
  };
  net_parser = ctx_parser_new (ctx, &config);
  net_server_task((void *)AF_INET);

  ctx_parser_destroy (net_parser);
  ctx_destroy (ctx);

  return 0;
}
