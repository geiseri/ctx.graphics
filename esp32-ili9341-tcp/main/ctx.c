
//#define CTX_WIFI_NAME     "wifi-name"
//#define CTX_WIFI_PASSWORD "password"

#pragma GCC optimize("jump-tables,tree-switch-conversion")

#define TOUCH_CST816S 0

#define DISPLAY_WIDTH  240
#define DISPLAY_HEIGHT 320

#define SCRATCH_BUF_BYTES (42 * 1024)
#define CTX_PTY 0
#define CTX_ESP 1
#define CTX_DITHER 1
#define CTX_PROTOCOL_U8_COLOR 1
#define CTX_LIMIT_FORMATS 1
#define CTX_32BIT_SEGMENTS 0
#define CTX_RASTERIZER 1
#define CTX_RASTERIZER_AA 3
#define CTX_ENABLE_GRAY1 1
#define CTX_ENABLE_GRAY2 1
#define CTX_ENABLE_GRAY4 1
#define CTX_ENABLE_GRAY8 1
#define CTX_ENABLE_RGB332 1
#define CTX_ENABLE_RGB565 1
#define CTX_ENABLE_RGB565_BYTESWAPPED 1
#define CTX_COMPOSITING_GROUPS 0
#define CTX_ALWAYS_USE_NEAREST_FOR_SCALE1 1
#define CTX_EVENTS 1
#define CTX_FORCE_INLINES 1
#define CTX_THREADS 1
#define CTX_BAREMETAL 1
#define CTX_ONE_FONT_ENGINE 1

#define CTX_MAX_SCANLINE_LENGTH 480
#define CTX_MAX_FRAMEBUFFER_WIDTH CTX_MAX_SCANLINE_LENGTH
#define CTX_MAX_JOURNAL_SIZE (1024 * 512)
// is also max and limits complexity
// of paths that can be filled
#define CTX_MIN_EDGE_LIST_SIZE 512

#define CTX_HASH_COLS 4
#define CTX_HASH_ROWS 4

#define CTX_MAX_DASHES 32
#define CTX_MAX_GRADIENT_STOPS 10
#define CTX_MAX_STATES 10
#define CTX_MAX_EDGES 127
#define CTX_MAX_PENDING 64

#define CTX_GRADIENT_CACHE_ELEMENTS 128
#define CTX_RASTERIZER_MAX_CIRCLE_SEGMENTS 64
#define CTX_MAX_KEYDB 16
#define CTX_MAX_TEXTURES 16
#define CTX_PARSER 1
#define CTX_PARSER_MAXLEN 512
#define CTX_PARSER_FIXED_TEMP 1
#define CTX_MAX_DEVICES 1
#define CTX_MAX_KEYBINDINGS 16
#define CTX_MAX_CBS 8
#define CTX_MAX_LISTEN_FDS 1
#define CTX_TERMINAL_EVENTS 1
#define CTX_FRAGMENT_SPECIALIZE 1
#define CTX_GSTATE_PROTECT 1
#define CTX_COMPOSITE_O2 1
#define CTX_RASTERIZER_SWITCH_DISPATCH 0
#define CTX_NATIVE_GRAYA8 1
#define CTX_AVOID_CLIPPED_SUBDIVISION 1


#include <stdint.h>
#include <stdarg.h>
#include "esp_event.h"
#include "esp_log.h"
#include "esp_system.h"
#include "esp_wifi.h"

#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "freertos/task.h"
#include "nvs_flash.h"

#include "lwip/err.h"
#include "lwip/sys.h"

#define CTX_IMPLEMENTATION
#include "ctx.h"

#include "driver/i2c.h"

#define TOUCH_CS 33
#define TOUCH_MHZ 25
#define LCD_PIN_SCLK 14
#define LCD_PIN_MOSI 13
#define LCD_PIN_MISO 12
#define LCD_PIN_DC 2
#define LCD_PIN_CS 15
#define LCD_PIN_BACKLIGHT 27
#define LCD_MHZ 55

#include "driver/spi_master.h"

#include "driver/gpio.h"
#include "esp_err.h"
#include "esp_lcd_ili9341.h"
#include "esp_lcd_panel_io.h"
#include "esp_lcd_panel_ops.h"
#include "esp_lcd_panel_vendor.h"
#include "esp_log.h"

static const char *TAG = "example";

static uint8_t scratch[SCRATCH_BUF_BYTES];

esp_lcd_panel_handle_t panel_handle = NULL;

static int fb_ready = 1;

static bool lcd_flush_ready(esp_lcd_panel_io_handle_t panel_io,
                            esp_lcd_panel_io_event_data_t *edata,
                            void *user_ctx) {
  fb_ready = 1;
  return false;
}

void renderer_idle (Ctx *ctx, void *user_data)
{
  vTaskDelay(1);
}

static int renderer_init (Ctx *ctx, void *user_data) {

#define EXAMPLE_LCD_BK_LIGHT_ON_LEVEL 1
  gpio_config_t bk_gpio_config = {.mode = GPIO_MODE_OUTPUT,
                                  .pin_bit_mask = 1ULL << LCD_PIN_BACKLIGHT};
  ESP_ERROR_CHECK(gpio_config(&bk_gpio_config));
  gpio_set_level(LCD_PIN_BACKLIGHT, EXAMPLE_LCD_BK_LIGHT_ON_LEVEL);

  spi_bus_config_t buscfg = {
      .sclk_io_num = LCD_PIN_SCLK,
      .mosi_io_num = LCD_PIN_MOSI,
      .miso_io_num = LCD_PIN_MISO,
      .quadwp_io_num = -1,
      .quadhd_io_num = -1,
      .max_transfer_sz = SCRATCH_BUF_BYTES,
  };
  ESP_ERROR_CHECK(spi_bus_initialize(SPI2_HOST, &buscfg, SPI_DMA_CH_AUTO));

  ESP_LOGI(TAG, "Install panel IO");
  esp_lcd_panel_io_handle_t io_handle = NULL;
  esp_lcd_panel_io_spi_config_t io_config = {
      .dc_gpio_num = LCD_PIN_DC,
      .cs_gpio_num = LCD_PIN_CS,
      .pclk_hz = LCD_MHZ * 1000000,
      .lcd_cmd_bits = 8,
      .lcd_param_bits = 8,
      .spi_mode = 0,
      .trans_queue_depth = 10,
      .on_color_trans_done = lcd_flush_ready,
      //.user_ctx = &disp_drv,
  };
  // Attach the LCD to the SPI bus
  ESP_ERROR_CHECK(esp_lcd_new_panel_io_spi((esp_lcd_spi_bus_handle_t)SPI2_HOST,
                                           &io_config, &io_handle));

  esp_lcd_panel_dev_config_t panel_config = {
      .reset_gpio_num = -1, // EXAMPLE_PIN_NUM_LCD_RST,
      .rgb_endian = LCD_RGB_ENDIAN_BGR,
      .bits_per_pixel = 16,
  };
  ESP_LOGI(TAG, "Install ILI9341 panel driver");
  ESP_ERROR_CHECK(
      esp_lcd_new_panel_ili9341(io_handle, &panel_config, &panel_handle));

  ESP_ERROR_CHECK(esp_lcd_panel_reset(panel_handle));
  ESP_ERROR_CHECK(esp_lcd_panel_init(panel_handle));
  ESP_ERROR_CHECK(esp_lcd_panel_mirror(panel_handle, true, false));
  ESP_ERROR_CHECK(esp_lcd_panel_disp_on_off(panel_handle, true));

  return 0;
}

void ctx_set_pixels(Ctx *ctx, void *user_data, int x, int y, int w, int h,
                    void *buf) {
  fb_ready = 0;
  esp_lcd_panel_draw_bitmap(panel_handle, x, y, x + w, y + h, buf);
  while(!fb_ready) vTaskDelay(1);
}

#include "wifi.h"

Ctx *ctx_host(void) {
  static Ctx *ctx = NULL;
  if (ctx)
    return ctx;


  CtxCbConfig config = {
    .format = CTX_FORMAT_RGB565_BYTESWAPPED,
    .renderer_init = renderer_init,
    .renderer_idle = renderer_idle,
    .set_pixels = ctx_set_pixels,
    .buffer_size = sizeof(scratch),
    .buffer = scratch,
    .flags = CTX_FLAG_HASH_CACHE|CTX_FLAG_RENDER_THREAD, //); //|CTX_FLAG_KEEP_DATA);
  };

  ctx = ctx_new_cb(DISPLAY_WIDTH, DISPLAY_HEIGHT, &config);

  ctx_start_frame(ctx);
  ctx_gray(ctx, 0);
  ctx_paint(ctx);
  ctx_logo(ctx, ctx_width(ctx)/2, ctx_height(ctx)/2, ctx_height(ctx)/4);
  ctx_end_frame(ctx);

  wifi_init_sta("peregrination","foofoofoo");

  return ctx;
}
