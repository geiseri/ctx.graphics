#pragma GCC optimize("jump-tables,tree-switch-conversion")

#include "port_config.h"
#define TAG "ctx"

#define SCRATCH_BUF_BYTES (32 * 1024)
#define CTX_VT 0
#define CTX_PTY 0
#define CTX_HASH_COLS 4
#define CTX_HASH_ROWS 4
#define CTX_DITHER 1
#define CTX_PARSER 1
#define CTX_ENABLE_CM 0
#define CTX_FORMATTER 0
#define CTX_PROTOCOL_U8_COLOR 1
#define CTX_LIMIT_FORMATS 1
#define CTX_32BIT_SEGMENTS 0
#define CTX_RASTERIZER 1
#define CTX_RASTERIZER_AA 3
#define CTX_ENABLE_RGB332 0
#define CTX_ENABLE_RGB565 1
#define CTX_ENABLE_RGB565_BYTESWAPPED 1
#define CTX_COMPOSITING_GROUPS 0
#define CTX_GROUP_MAX 1
#define CTX_ALWAYS_USE_NEAREST_FOR_SCALE1 1
#define CTX_EVENTS 1
#define CTX_FORCE_INLINES 0
#define CTX_THREADS 1
#define CTX_BAREMETAL 1
#define CTX_ONE_FONT_ENGINE 1
#define CTX_MAX_FONTS 3

#define CTX_MAX_SCANLINE_LENGTH 480
#define CTX_MAX_FRAMEBUFFER_WIDTH CTX_MAX_SCANLINE_LENGTH
#define CTX_MIN_JOURNAL_SIZE (1024)
#define CTX_MAX_JOURNAL_SIZE (2048)


// is also max and limits complexity
// of paths that can be filled
#define CTX_MIN_EDGE_LIST_SIZE   1024

#define CTX_MAX_DASHES 32
#define CTX_MAX_GRADIENT_STOPS 10
#define CTX_MAX_STATES 16
#define CTX_MAX_EDGES 127
#define CTX_MAX_PENDING 64
#define CTX_VT_STYLE_SIZE 32
#define CTX_GRADIENT_CACHE 1
#define CTX_GRADIENT_CACHE_ELEMENTS 128
#define CTX_RASTERIZER_MAX_CIRCLE_SEGMENTS 64
#define CTX_MAX_KEYDB 32
#define CTX_MAX_TEXTURES 16
#define CTX_PARSER_MAXLEN 512
#define CTX_PARSER_FIXED_TEMP 0
#define CTX_MAX_DEVICES 1
#define CTX_MAX_KEYBINDINGS 24
#define CTX_MAX_CBS 32 // max defined interaction listeners
#define CTX_MAX_LISTEN_FDS 1
#define CTX_TERMINAL_EVENTS 1
#define CTX_FRAGMENT_SPECIALIZE 1      // more optimize texture|gradients
#define CTX_GSTATE_PROTECT 1
#define CTX_COMPOSITE_O2 1
#define CTX_RASTERIZER_O2 0
#define CTX_NATIVE_GRAYA8 0
#define CTX_AVOID_CLIPPED_SUBDIVISION 0

#include <stdint.h>

#define _CTX_INTERNAL_FONT_

#include "Roboto-Regular.h"
#include "Cousine-Regular.h"
#define CTX_STATIC_FONT(font)                                                  \
  ctx_load_font_ctx(ctx_font_##font##_name, ctx_font_##font,                   \
                    sizeof(ctx_font_##font))

#define CTX_FONT_1 CTX_STATIC_FONT(Roboto_Regular)
#define CTX_FONT_2 CTX_STATIC_FONT(Cousine_Regular)

#define CTX_IMPLEMENTATION
#include "ctx.h"

#include "driver/i2c.h"
#include "esp_lcd_touch_cst816s.h"
#include "gc9a01.h"

#include "esp_event.h"
#include "esp_log.h"
#include "esp_system.h"
#include "esp_wifi.h"
#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "freertos/task.h"
#include "nvs_flash.h"
#include <string.h>

#include "lwip/err.h"
#include "lwip/sys.h"

static uint8_t scratch[SCRATCH_BUF_BYTES];
static esp_lcd_touch_handle_t tp = NULL;

static void touch_init() {
  esp_lcd_panel_io_handle_t tp_io_handle = NULL;

  i2c_config_t i2c_conf = {
      .mode = I2C_MODE_MASTER,
      .sda_io_num = 4,
      .scl_io_num = 5,
      .sda_pullup_en = GPIO_PULLUP_ENABLE,
      .scl_pullup_en = GPIO_PULLUP_ENABLE,
      .master.clk_speed = 400000, // 400kHz
  };
  ESP_ERROR_CHECK(i2c_param_config(I2C_NUM_0, &i2c_conf));
  ESP_ERROR_CHECK(i2c_driver_install(I2C_NUM_0, i2c_conf.mode, 0, 0, 0));

  esp_lcd_panel_io_i2c_config_t tp_io_config =
      ESP_LCD_TOUCH_IO_I2C_CST816S_CONFIG();
  // ESP_LOGI(TAG, "Initialize touch IO (I2C)");
  ESP_ERROR_CHECK(esp_lcd_new_panel_io_i2c((esp_lcd_i2c_bus_handle_t)I2C_NUM_0,
                                           &tp_io_config, &tp_io_handle));

  esp_lcd_touch_config_t tp_cfg = {
      .x_max = DISPLAY_WIDTH,
      .y_max = DISPLAY_HEIGHT,
      .rst_gpio_num = 1,
      .int_gpio_num = 0,
      .levels =
          {
              .reset = 0,
              .interrupt = 0,
          },
      .flags =
          {
              .swap_xy = 0,
              .mirror_x = 0,
              .mirror_y = 0,
          },
  };

  ESP_ERROR_CHECK(esp_lcd_touch_new_i2c_cst816s(tp_io_handle, &tp_cfg, &tp));
}

static void consume_events (Ctx *ctx, void *user_data) {
  esp_lcd_touch_read_data(tp);
  static uint16_t touch_x[1];
  static uint16_t touch_y[1];
  static uint16_t touch_strength[1];
  uint8_t touch_cnt = 0;
  bool touchpad_pressed = esp_lcd_touch_get_coordinates(
      tp, touch_x, touch_y, touch_strength, &touch_cnt, 1);
  float x = touch_x[0];
  float y = touch_y[0];
  x -= 10;
  y -= 10;
  if (y < 0)
    y = 0;
  if (x < 0)
    x = 0;
  y *= 1.07;
  x *= 1.05;
  if (y > DISPLAY_HEIGHT)
    y = DISPLAY_HEIGHT;
  if (x > DISPLAY_WIDTH)
    x = DISPLAY_WIDTH;

  static bool was_pressed = false;
  if (touchpad_pressed) {
    if (!was_pressed)
      ctx_pointer_press(ctx, x, y, 0, 0);
    else
      ctx_pointer_motion(ctx, x, y, 0, 0);
  } else if (was_pressed) {
    ctx_pointer_release(ctx, x, y, 0, 0);
  }
  was_pressed = touchpad_pressed;

}

void esp_backlight(int percent) {
  if (percent < 7)
    percent = 7;
  GC9A01_SetBL(percent);
}

static void lcd_init(void) {
  GC9A01_Init();
  esp_backlight(30);
}

void ctx_set_pixels(Ctx *ctx, void *user_data, int x, int y, int w, int h,
                    void *buf) {
  uint8_t *pixels = (uint8_t *)buf;
  GC9A01_SetWindow(x, y, x + w - 1, y + h - 1);
  lcd_data(pixels, w * h * 2);

}

void renderer_idle (Ctx *ctx, void *user_data)
{
  vTaskDelay(1);
}

#include "driver/uart.h"
#include "driver/usb_serial_jtag.h"
#include "esp_vfs.h"
#include "esp_vfs_dev.h"
#include "esp_vfs_fat.h"
#include "esp_vfs_usb_serial_jtag.h"

void usb_serial_jtag_init(void) {
  /* Disable buffering on stdin */

  /* Minicom, screen, idf_monitor send CR when ENTER key is pressed */
  esp_vfs_dev_usb_serial_jtag_set_rx_line_endings(ESP_LINE_ENDINGS_CR);
  /* Move the caret to the beginning of the next line on '\n' */
  esp_vfs_dev_usb_serial_jtag_set_tx_line_endings(ESP_LINE_ENDINGS_CRLF);

  /* Enable non-blocking mode on stdin and stdout */
  usb_serial_jtag_driver_config_t usb_serial_jtag_config =
      USB_SERIAL_JTAG_DRIVER_CONFIG_DEFAULT();

  esp_err_t ret = ESP_OK;
  /* Install USB-SERIAL-JTAG driver for interrupt-driven reads and writes */
  ret = usb_serial_jtag_driver_install(&usb_serial_jtag_config);
  if (ret != ESP_OK) {
    printf("console eek!\n");
    return;
  }

  esp_vfs_usb_serial_jtag_use_driver();
  //esp_vfs_usb_serial_jtag_use_nonblocking();
#if 0
    setvbuf(stdin, NULL, _IONBF, 0);
    setvbuf(stdout, NULL, _IONBF, 0);
  fcntl(fileno(stdout), F_SETFL, 0);
  fcntl(fileno(stdin), F_SETFL, 0);
#endif
}

void flash_init(void) {
  // Handle of the wear levelling library instance
  static wl_handle_t s_wl_handle = WL_INVALID_HANDLE;
  const esp_vfs_fat_mount_config_t mount_config = {
      .max_files = 4,
      .format_if_mount_failed = true,
      .allocation_unit_size = CONFIG_WL_SECTOR_SIZE};
  esp_err_t err = esp_vfs_fat_spiflash_mount_rw_wl("/flash", "vfs",
                                                   &mount_config, &s_wl_handle);
  if (err != ESP_OK) {
    ESP_LOGE(TAG, "Failed to mount FATFS (%s)", esp_err_to_name(err));
  }
}

#include "wifi.h"


Ctx *ctx_host(void) {
  static Ctx *ctx = NULL;
  if (ctx)
    return ctx;

  //  uart_init();
  flash_init();
  usb_serial_jtag_init();
  lcd_init();
  touch_init();
  CtxCbConfig config = {
	  .format = CTX_FORMAT_RGB565_BYTESWAPPED,
	  .set_pixels = ctx_set_pixels,
          .renderer_idle = renderer_idle,
	  .consume_events = consume_events,
	  .buffer_size = sizeof(scratch),
	  .buffer = scratch,
	  .flags = 0
              | CTX_FLAG_HASH_CACHE 
	    //| CTX_FLAG_LOWFI
            //| CTX_FLAG_RENDER_THREAD
  };
  ctx = ctx_new_cb (DISPLAY_WIDTH, DISPLAY_HEIGHT, &config);
  ctx_start_frame(ctx);
  ctx_gray(ctx, 0);
  ctx_paint(ctx);
  ctx_logo(ctx, ctx_width(ctx)/2, ctx_height(ctx)/2, ctx_height(ctx)/4);
  ctx_end_frame(ctx);

  wifi_init_sta(CTX_WIFI_NAME, CTX_WIFI_PASSWORD);
  return ctx;
}

