#define CTX_PTY            0
#define CTX_ESP            1
#define CTX_IMPLEMENTATION 1
#define CTX_32BIT_SEGMENTS 0
#define CTX_RASTERIZER     1
#define CTX_RASTERIZER_AA  3
#define CTX_THREADS        1
#define CTX_EVENTS         1
#define CTX_COMPOSITE_O3   1
#define CTX_RASTERIZER_O2  1

#include "ctx.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "nvs_flash.h"
#include "nvs.h"
#include "esp_log.h"
#include "esp_err.h"
#include "esp_check.h"
#include "esp_memory_utils.h"
#include "esp_system.h"
#include "bsp/esp-bsp.h"
#include "bsp/display.h"
#include "bsp/touch.h"

static esp_lcd_panel_handle_t panel;

static void set_pixels(Ctx *ctx, void *user_data, int x, int y, int w, int h,
                    void *buf) {
   esp_lcd_panel_draw_bitmap(panel, x, y, x + w, y + h, buf);
}

esp_lcd_touch_handle_t touch_handle;

static int renderer_init (Ctx *ctx, void *user_data)
{
  esp_lcd_panel_io_handle_t io;
  bsp_display_new (NULL, &panel, &io);
  bsp_touch_new (NULL, &touch_handle);
  //esp_lcd_panel_disp_on_off (panel, 1);
  return 0;
}

static void renderer_idle (Ctx *ctx, void *user_data)
{
  vTaskDelay(1);
}

typedef struct Touch {
  int  id;
  int  old_id;
  int  x;
  int  y;
} Touch;

#define MAX_TOUCHES 5
static Touch touches[MAX_TOUCHES]={0,};

static int touch_find_id (Ctx *ctx, int x, int y, int *pos)
{
#define POW2(a) ((a)*(a))
  int min_sq_dist = POW2(2048);
  int min_no = -1;
  int available = -1;

  for (int i = 0; i < MAX_TOUCHES; i++)
  {
     if (touches[i].old_id)
     {
       int sq_dist = POW2(x-touches[i].x) + POW2(y-touches[i].y);
       if (sq_dist < min_sq_dist)
       {
         min_sq_dist = sq_dist;
         min_no = i;
       }
     }
     else
     available = i;
  }

  // the distance that is recognized as the same touch-point
  // and given consistent value.

  if (min_sq_dist < POW2(80))
  {
    *pos = min_no;
    return touches[min_no].old_id;
  }

  int found = 1;
  int id = 4;
  for (id = 4; id < 4 + MAX_TOUCHES && found; id++)
  {
    found = 0;
    for (int i = 0; i < MAX_TOUCHES; i++)
      if (touches[i].old_id == id)
        found = 1;
    if (!found)
      break;
  }
  if (!found)
  {
    *pos = available;
    return id;
  }
  return 0;
}

static void consume_events (Ctx *ctx, void *user_data) {
  uint16_t x[MAX_TOUCHES];
  uint16_t y[MAX_TOUCHES];
  uint16_t strength[MAX_TOUCHES];
  uint8_t  point_num;
  esp_lcd_touch_read_data (touch_handle);
  bool res = esp_lcd_touch_get_coordinates(touch_handle, x,y,strength,&point_num, MAX_TOUCHES-1);

  for (int i = 0; i < MAX_TOUCHES; i++)
  {
    touches[i].old_id = touches[i].id;
    touches[i].id = 0;
  }
  if (res)
  {
    for (int i = 0; i < point_num; i++)
    {
      int pos = 0;
      int id = touch_find_id (ctx, x[i], y[i], &pos);
      if (id)
      {
        touches[pos].x = x[i];
        touches[pos].y = y[i];
        touches[pos].id = id;
      }
    }
  }
  for (int i = 0; i < MAX_TOUCHES; i++)
  {
    if (touches[i].id == 0 && touches[i].old_id)
      ctx_pointer_release (ctx, touches[i].x, touches[i].y, touches[i].old_id, 0);
    else if (touches[i].id && (touches[i].old_id == 0))
      ctx_pointer_press (ctx, touches[i].x, touches[i].y, touches[i].id, 0);
    else if (touches[i].id == touches[i].old_id && touches[i].id != 0)
      ctx_pointer_motion (ctx, touches[i].x, touches[i].y, touches[i].old_id, 0);
  }

  vTaskDelay(1);
}



Ctx *ctx_host (void)
{
  static Ctx *ctx = NULL;
  if (ctx)
    return ctx;
#if CONFIG_BSP_LCD_COLOR_FORMAT_RGB888
  int scratch_size = (BSP_LCD_H_RES * BSP_LCD_V_RES * 3);
  CtxPixelFormat format = CTX_FORMAT_BGR8;
#else
  int scratch_size = (BSP_LCD_H_RES * BSP_LCD_V_RES * 2);
  CtxPixelFormat format = CTX_FORMAT_RGB565;
#endif
  char *scratch = ctx_malloc (scratch_size);

  CtxCbConfig config =
  {

     .format = format,
     .set_pixels = set_pixels,
     .renderer_init = renderer_init,
     .renderer_idle = renderer_idle,
     .consume_events = consume_events,
     .buffer = scratch,
     .buffer_size = scratch_size,
     .flags = CTX_FLAG_HASH_CACHE | CTX_FLAG_SHOW_FPS | CTX_FLAG_RENDER_THREAD,
  };

  ctx = ctx_new_cb (BSP_LCD_H_RES, BSP_LCD_V_RES, &config);
  ctx_start_frame(ctx);
  ctx_gray(ctx, 0);
  ctx_paint(ctx);
  ctx_logo(ctx, ctx_width(ctx)/2, ctx_height(ctx)/2, (ctx_width(ctx)+ctx_height(ctx))/4);
  ctx_end_frame(ctx);
  bsp_display_backlight_on();

  return ctx;
}
