#include "ctx-split.h"

int ctx_frame_ack = -1;

#if CTX_NET

#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <netdb.h>

typedef struct _CtxNet CtxNet;
struct _CtxNet
{
   CtxBackend backend;
   int  flags;
   int  width;
   int  height;
   int  cols;
   int  rows;

   int sock; // client socket descriptor

   int in_fd;
   int out_fd;
};

//int ctx_frame_ack = -1;
#if CTX_FORMATTER

static int ctx_net_strout (CtxNet *net, const char *str)
{
  if (str)
    return write (net->out_fd, str, strlen (str));
  return 0;
}

static void ctx_net_end_frame (Ctx *ctx)
{
  CtxNet *net = (CtxNet*)ctx->backend;

  if (net->flags & CTX_FLAG_SYNC)
  {
    ctx_net_strout (net, "\033[5n\n");

#if CTX_EVENTS
    ctx_frame_ack = 0;
    static int time = 0;
    int bail_time = time + 500;
    //int wait_time = time + 100; // this make us aim for 10fps...
                                // we do this to avoid overwhelming uarts
    int wait_time = time + 50; // this make us aim for 20fps...
    //int wait_time = time + 25; // this make us aim for 40fps...
    do {
       ctx_consume_events (net->backend.ctx);
       time = ctx_ms(ctx);
    } while (time < wait_time || (ctx_frame_ack != 1 && time < bail_time));
#endif
  } else
  {
    usleep (1000 * 50);
    ctx_consume_events (net->backend.ctx);
  }

  ctx_net_strout (net, "\033[?201h\033[H\033[?25l\033[?200h:\n");

  ctx_render_fd (net->backend.ctx, net->out_fd, 0);//CTX_FORMATTER_FLAG_FLUSH);
  ctx_net_strout (net, " X\n");

}

void ctx_net_destroy (CtxNet *net)
{
  ctx_drain_fd (net->in_fd);
  ctx_net_strout (net, "\033[?25h"
                       "\033[?201l"
                       "\033[?47l"   // XTERM_ALTSCREEN_OFF
                       "\033[?1000l\033[?1003l" // TERMINAL_MOUSE_OFF
                       "\033[?200l"
                       "\033[?1049l");
  ctx_term_noraw (net->in_fd);

  if (net->sock)
    close (net->sock);
  ctx_free (net);
}

#if CTX_AUDIO
void ctx_ctx_pcm (Ctx *ctx);
#endif

static int ctx_net_has_data (CtxNet *net, int timeout)
{
  struct timeval tv = {0,0};
  fd_set rfds;
  FD_ZERO(&rfds);
  FD_SET(net->in_fd, &rfds);
  tv.tv_sec = 0;
  tv.tv_usec = timeout;
  return (select(net->in_fd+1, &rfds, NULL, NULL, &tv)>0);
}

static char *ctx_net_get_event (Ctx *ctx, int timeout)
{
  CtxNet *net = (CtxNet*)ctx->backend;
  int got_event = ctx_net_has_data (net, timeout);
  char buf[201];
  int length;

  if (got_event)
  for (length = 0; got_event && length < 128; length ++)
  {
    if (read (net->in_fd, &buf[length], 1) != -1)
      {
         buf[length+1] = 0;
         if (!strcmp ((char*)buf, "\033[0n"))
         {
           ctx_frame_ack = 1;
           return ctx_strdup ("ack");
         }
         else if (buf[length]=='\n')
         {
           buf[length]=0;
           return ctx_strdup (buf);
         }
      }
      got_event = ctx_net_has_data (net, timeout);
    }

  return ctx_strdup ("idle");
}

static void ctx_net_consume_events (Ctx *ctx)
{
  //int ix, iy;
  CtxNet *net = (CtxNet*)ctx->backend;
  char *event = NULL;
#if CTX_AUDIO
  //ctx_ctx_pcm (ctx);
#endif

    do {

      float x = 0, y = 0;
      int b = 0;
      char event_type[128]="";
      event = ctx_net_get_event (ctx, 1000/60);

      if (event)
      {
      sscanf (event, "%30s %f %f %i", event_type, &x, &y, &b);
      if (!strcmp (event_type, "idle") ||
          !strcmp (event_type, "ack"))
       
      {
        ctx_free (event);
        event = NULL;
      }
      else if (!strcmp (event_type, "pp"))
      {
        ctx_pointer_press (ctx, x, y, b, 0);
      }
      else if (!strcmp (event_type, "pd")||
               !strcmp (event_type, "pm"))
      {
        ctx_pointer_motion (ctx, x, y, b, 0);
      }
      else if (!strcmp (event_type, "pr"))
      {
        ctx_pointer_release (ctx, x, y, b, 0);
      }
      else if (!strcmp (event_type, "message"))
      {
        ctx_incoming_message (ctx, event + strlen ("message"), 0);
      } 
      else if (!strcmp (event, "size-changed"))
      {
        //fprintf (stdout, "\033[H\033[2J\033[?25l");fflush(stdout);
        net->cols = ctx_terminal_cols (net->in_fd, net->out_fd);
        net->rows = ctx_terminal_rows (net->in_fd, net->out_fd);

        ctx_set_size (ctx, ctx_terminal_width(net->in_fd, net->out_fd), ctx_terminal_height(net->in_fd, net->out_fd));

        ctx_queue_draw (ctx);

      //   ctx_key_press(ctx,0,"size-changed",0);
      }
      else if (!strcmp (event_type, "keyup"))
      {
        char buf[4]={ (int)x, 0 };
        ctx_key_up (ctx, (int)x, buf, 0);
      }
      else if (!strcmp (event_type, "keydown"))
      {
        char buf[4]={ (int)x, 0 };
        ctx_key_down (ctx, (int)x, buf, 0);
      }
      else
      {
        ctx_key_press (ctx, 0, event, 0);
      }

        if (event)
          ctx_free (event);
      }
    } while (event);
}


Ctx *ctx_new_net (int width, int height, int flags, const char *hostip, int port)
{
  float font_size = 12.0;
  CtxNet *net = (CtxNet*)ctx_calloc (1, sizeof (CtxNet));
  net->sock = socket (AF_INET, SOCK_STREAM,0);
  if (net->sock < 0)
  {
    fprintf (stderr, "failed to create socket\n");
    ctx_free (net);
    return NULL;
  }

  struct sockaddr_in  server_addr;
  server_addr.sin_family = AF_INET;
  server_addr.sin_port = htons (port);
  server_addr.sin_addr.s_addr = inet_addr (hostip);
  int retcode = connect (net->sock, (struct sockaddr *)&server_addr, sizeof(server_addr));
  if (retcode < 0)
  {
    fprintf(stderr, "*** ERROR - connect() failed \n");
    ctx_free (net);
    return NULL;
  }

  CtxBackend *backend = (CtxBackend*)net;

  net->in_fd = net->out_fd = net->sock;

  retcode = ctx_net_strout (net, "\033[?1049h\033[?200h\033[?201h");
  if (retcode < 0)
  {
    fprintf(stderr, "*** ERROR - initial send() failed \n");
    ctx_free (net);
    return NULL;
  }

  net->flags = flags;
  if (width <= 0 || height <= 0)
  {
    width  = net->width = ctx_terminal_width (net->in_fd, net->out_fd);
    height = net->height = ctx_terminal_height (net->in_fd, net->out_fd);
    net->cols = ctx_terminal_cols (net->in_fd, net->out_fd);
    net->rows = ctx_terminal_rows (net->in_fd, net->out_fd);
    font_size = height / net->rows;
    //fprintf (stderr, "got dim: %ix%i fs:%f rows:%i\n", width, height, font_size, net->rows);
  }
  else
  {
    net->width = width;
    net->height = height;
    net->cols   = width / 80;
    net->rows   = height / 24;
  }
  Ctx *ctx = _ctx_new_drawlist (width, height);


  backend->ctx = ctx;
  backend->end_frame = ctx_net_end_frame;
  backend->type = CTX_BACKEND_CTX;
  backend->destroy = (void(*)(void *))ctx_net_destroy;
  backend->process = (void(*)(Ctx *a, const CtxCommand *c))ctx_drawlist_process;
  backend->consume_events = ctx_net_consume_events;
  ctx_set_backend (ctx, net);
  ctx_set_size (ctx, width, height);
  ctx_font_size (ctx, font_size);
  return ctx;
}

Ctx *ctx_new_fds (int width, int height, int in_fd, int out_fd, int flags)
{
  float font_size = 12.0;
  Ctx *ctx = _ctx_new_drawlist (width, height);
  CtxNet *net = (CtxNet*)ctx_calloc (1, sizeof (CtxNet));
  CtxBackend *backend = (CtxBackend*)net;
  net->in_fd = in_fd;
  net->out_fd = out_fd;
  
  ctx_term_raw (net->in_fd);
  //ctx_net_strout (net, "\033[?47h\033[?200h\033[?201h");
  net->flags = flags;
  if (width <= 0 || height <= 0)
  {
    width  = net->width = ctx_terminal_width (net->in_fd, net->out_fd);
    height = net->height = ctx_terminal_height (net->in_fd, net->out_fd);
    net->cols = ctx_terminal_cols (net->in_fd, net->out_fd);
    net->rows = ctx_terminal_rows (net->in_fd, net->out_fd);
    font_size = height / net->rows;
  }
  else
  {
    net->width = width;
    net->height = height;
    net->cols   = width / 80;
    net->rows   = height / 24;
  }
  ctx_term_raw (net->in_fd);
  ctx_net_strout (net, "\033[?1049h\n\033[?201h\n");

  backend->ctx = ctx;
  backend->end_frame = ctx_net_end_frame;
  backend->type = CTX_BACKEND_CTX;
  backend->destroy = (void(*)(void *))ctx_net_destroy;
  backend->process = (void(*)(Ctx *a, const CtxCommand *c))ctx_drawlist_process;
  backend->consume_events = ctx_net_consume_events;
  ctx_set_backend (ctx, net);
  ctx_set_size (ctx, width, height);
  ctx_font_size (ctx, font_size);
  return ctx;
}

Ctx *ctx_new_ctx (int width, int height, int flags)
{
  return ctx_new_fds (width, height, STDIN_FILENO, STDOUT_FILENO, flags);
}

#endif

#endif
