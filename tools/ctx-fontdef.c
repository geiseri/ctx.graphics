#include <libgen.h>
#include <stdio.h>
#include <stdlib.h>

#define CTX_MAX_DRAWLIST_SIZE 4096000
#define CTX_BACKEND_TEXT 0 // we keep then non-backend code paths
                           // for code handling aroud, this should
                           // be run-time to permit doing text_to_path
#define CTX_RASTERIZER 1

#define CTX_BITPACK_PACKER 1 // pack vectors
#define CTX_BITPACK 1
#define STB_TRUETYPE_IMPLEMENTATION
#include "stb_truetype.h"
#include <sys/time.h>
#define CTX_EXTRAS 1
#define CTX_FONTS_FROM_FILE 1
#define CTX_AVX2 0
#define CTX_IMPLEMENTATION
#define CTX_PARSER 1
#include "ctx.h"

static int usage()
{
  fprintf(stderr, "A tool to generate a struct of native ctx embedded font format\n");
  fprintf(stderr, "The resulting define will be sent to stdout");
  fprintf(stderr, "\n");
  fprintf(stderr, "usage: ctx-fontdef <file.ttf> <variable_name> [chars]\n");
  return -1;
}

CtxDrawlist output_font = {
    NULL,
};
uint32_t glyphs[65536];
unsigned int n_glyphs = 0;

void add_glyph_real(Ctx *ctx, uint32_t glyph)
{
  for (unsigned int i = 0; i < n_glyphs; i++)
  {
    if (glyphs[i] == glyph)
    {
      return;
    }
  }
  ctx_start_frame(ctx);
  ctx_font_size(ctx, CTX_BAKE_FONT_SIZE);
  ctx_move_to(ctx, 0, 0);
  if (ctx_glyph_unichar(ctx, glyph, 0))
  {
    char buf[44] = {0, 0, 0, 0, 0};
    ctx_unichar_to_utf8(glyph, (uint8_t *)buf);
    fprintf(stderr, "Warning \"%s\" was not found in font!\n", buf);
    return;
  }
  glyphs[n_glyphs++] = glyph;
  ctx->drawlist.flags = CTX_TRANSFORMATION_BITPACK;
  ctx_drawlist_compact(&ctx->drawlist);
  // char buf[44]={0,0,0,0,0};
  uint32_t args[2] = {glyph, ctx_glyph_width(ctx, ctx_glyph_lookup(ctx, glyph)) * 256};
  ctx_drawlist_add_u32(&output_font, CTX_DEFINE_GLYPH, args);

  for (unsigned int i = 3; i < ctx->drawlist.count - 1; i++)
  {
    CtxEntry *entry = &ctx->drawlist.entries[i];
    args[0] = entry->data.u32[0];
    args[1] = entry->data.u32[1];
    ctx_drawlist_add_u32(&output_font, entry->code, &args[0]);
  }
}

uint32_t incoming_glyphs[65536];
unsigned int n_incoming_glyphs = 0;

void add_glyph(Ctx *ctx, uint32_t glyph)
{
  for (unsigned int i = 0; i < n_incoming_glyphs; i++)
  {
    if (incoming_glyphs[i] == glyph)
    {
      fprintf(stderr, "Duplicate glyph %d\n", glyph);
      return;
    }
  }
  incoming_glyphs[n_incoming_glyphs++] = glyph;
}

int compare_glyphs(const void *a, const void *b)
{
  uint32_t au = ((uint32_t *)a)[0];
  uint32_t bu = ((uint32_t *)b)[0];
  return au - bu;
}

void real_add_glyphs(Ctx *ctx)
{
  qsort(incoming_glyphs, n_incoming_glyphs, sizeof(uint32_t),
        compare_glyphs);
  for (unsigned int i = 0; i < n_incoming_glyphs; i++)
    if (incoming_glyphs[i] &&
        incoming_glyphs[i] != '\r' &&
        incoming_glyphs[i] != '\t' &&
        incoming_glyphs[i] != 2 &&
        incoming_glyphs[i] != '\b')
    {
      add_glyph_real(ctx, incoming_glyphs[i]);
    }
}

static int find_glyph(CtxDrawlist *drawlist, uint32_t unichar)
{
  for (unsigned int i = 0; i < drawlist->count; i++)
  {
    if (drawlist->entries[i].code == CTX_DEFINE_GLYPH &&
        drawlist->entries[i].data.u32[0] == unichar)
    {
      return i;
      // XXX this could be prone to insertion of valid header
      // data in included bitmaps.. is that an issue?
    }
  }
  fprintf(stderr, "Eeeek %i\n", unichar);
  return -1;
}

int main(int argc, char **argv)
{

  const char *subsets = NULL;

  Ctx *ctx;

  if (argc < 3)
  {
    return usage();
  }
  if (argc == 3)
  {
    subsets = argv[3];
  }

  const char *name = argv[2];
  FILE *outfile = NULL;
  outfile = stdout;

  int font_no = ctx_load_font_ttf_file("import", argv[1]);

  ctx = ctx_new(1000, 1000, "drawlist");
  _ctx_set_transformation(ctx, CTX_TRANSFORMATION_RELATIVE);
  ctx_font(ctx, "import");

  const char *font_name = ctx_get_font_name(NULL, font_no);

  if (subsets == NULL)
  {
    for (int glyph = 0; glyph < 127; glyph++)
    {
      add_glyph(ctx, glyph);
    }
  }
  else
  {
    for (const char *utf8 = subsets; *utf8; utf8 = ctx_utf8_skip(utf8, 1))
    {
      add_glyph(ctx, ctx_utf8_to_unichar(utf8));
    }
  }

  {
    CtxEntry entry;
    entry.code = CTX_DEFINE_FONT;
    entry.data.u8[0] = CTX_SUBDIV;
    entry.data.u8[1] = CTX_BAKE_FONT_SIZE;
    entry.data.u32[1] = 23; // length
    ctx_drawlist_add_single(&output_font, &entry);
  }

  char temp_name[256];
  sprintf(temp_name, " %s", font_name);
  ctx_drawlist_add_data(&output_font, temp_name, strlen(temp_name) + 1);

  real_add_glyphs(ctx);

  for (unsigned int i = 0; i < n_glyphs; i++)
    for (unsigned int j = 0; j < n_glyphs; j++)
    {
      float kerning = ctx_glyph_kern(ctx, glyphs[i], glyphs[j]);
      if (fabsf(kerning) > 0.01)
      {
        CtxCommand command;
        unsigned int pos = find_glyph(&output_font, glyphs[i]);
        pos++;
        while (pos < output_font.count && output_font.entries[pos].code != CTX_DEFINE_GLYPH)
        {
          pos++;
        }
        command.code = CTX_KERNING_PAIR;
        command.kern.glyph_before = glyphs[i];
        command.kern.glyph_after = glyphs[j];
        command.kern.amount = kerning * 256;
        ctx_drawlist_insert_entry(&output_font, pos, (CtxEntry *)&command);
      }
    }

  ctx_destroy(ctx);

  output_font.entries[0].data.u32[1] = output_font.count;

  fprintf(outfile, "/* glyph index for %s: \n", font_name);
  int col = 5;
  for (unsigned int i = 0; i < output_font.count; i++)
  {
    CtxEntry *entry = &output_font.entries[i];
    if (entry->code == '@')
    {
      char buf[44] = {0, 0, 0, 0, 0};
      ctx_unichar_to_utf8(entry->data.u32[0], (uint8_t *)buf);
      switch (buf[0])
      {
      case '\\':
        fprintf(outfile, "\\");
        break;
      default:
        fprintf(outfile, "%s", buf);
      }
      col++;
      if (col > 80)
      {
        col = 5;
        fprintf(outfile, "\n  ");
      }
    }
  }

  fprintf(outfile, "\n*/\n");
  fprintf(outfile, "static const char %s_name[] = \"%s\";\n", name, font_name);
  fprintf(outfile, "static const struct __attribute__ ((packed)) {uint8_t code; uint32_t a; uint32_t b;}\n%s[]={\n", name);

  for (unsigned int i = 0; i < output_font.count; i++)
  {
    CtxEntry *entry = &output_font.entries[i];

    if (entry->code == 15)
    {
      fprintf(outfile, "{%i, 0x%08x, 0x%08x},", entry->code,
              0, // XXX : why did it contain garbage?
              entry->data.u32[1]);
    }
    else if (entry->code > 32 && entry->code < 127)
    {
      fprintf(outfile, "{'%c', 0x%08x, 0x%08x},", entry->code,
              entry->data.u32[0],
              entry->data.u32[1]);
    }
    else
    {
      fprintf(outfile, "{%i, 0x%08x, 0x%08x},", entry->code,
              entry->data.u32[0],
              entry->data.u32[1]);
    }
    if (entry->code == '@')
    {
      char buf[44] = {0, 0, 0, 0, 0};
      ctx_unichar_to_utf8(entry->data.u32[0], (uint8_t *)buf);
      switch (buf[0])
      {
      case '\\':
        fprintf(outfile, "/*       \\         x-advance: %f */", entry->data.u32[1] / 256.0);
        break;
      default:
        fprintf(outfile, "/*        %s        x-advance: %f */", buf, entry->data.u32[1] / 256.0);
      }
    }
    else if (entry->code == 15)
    {
      fprintf(outfile, "/* length:%i CTX_SUBDIV:%i CTX_BAKE_FONT_SIZE:%i */",
              entry->data.u32[1],
              entry->data.u8[0],
              entry->data.u8[1]);
    }
    else if (entry->code == '(')
    {
      char *str = (char *)(entry + 1);
      fprintf(outfile, "/*");
      for (int i = 0; str[i]; i++)
      {
        if (str[i] == '\n')
        {
          fprintf(outfile, "\n                                ");
        }
        else
        {
          fprintf(outfile, "%c", str[i]);
        }
      }
      fprintf(outfile, "*/");
    }
    else if (entry->code == '[')
    {
      char buf[44] = {0, 0, 0, 0, 0};
      fprintf(outfile, "/*kerning  ");
      ctx_unichar_to_utf8(entry->data.u16[0], (uint8_t *)buf);
      fprintf(outfile, "%s ", buf);
      ctx_unichar_to_utf8(entry->data.u16[1], (uint8_t *)buf);
      fprintf(outfile, "%s : %f ", buf, entry->data.s32[1] / 255.0f);
      fprintf(outfile, "*/");
    }
    else
    {
    }
    fprintf(outfile, "\n");
  }
  fprintf(outfile, "};\n");

  fclose(outfile);
  return 0;
}
